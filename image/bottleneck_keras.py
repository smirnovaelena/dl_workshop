import os
import numpy as np
from keras.models import Sequential
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras import optimizers
from keras.applications.vgg16 import VGG16

# Keras VGG16 input shape
img_width, img_height = 150, 150

data_name = "cat_vs_dog"
data_dir = '/home/ubuntu/datasets/image/' + data_name
train_data_dir = data_dir + '/train'
validation_data_dir = data_dir + '/validation'
nb_train_samples = 2048 
nb_validation_samples = 832 

datagen = ImageDataGenerator(rescale=1./255)

train_generator_bottleneck = datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode=None,
        shuffle=False)

validation_generator_bottleneck = datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode=None,
        shuffle=False)

model_vgg = VGG16(include_top=False, weights='imagenet', input_shape=(img_width, img_height, 3))
        
bottleneck_features_train = model_vgg.predict_generator(train_generator_bottleneck, nb_train_samples)
np.save(open(data_name + '_bottleneck_features_train.npy', 'wb'), bottleneck_features_train)
bottleneck_features_validation = model_vgg.predict_generator(validation_generator_bottleneck, nb_validation_samples)
np.save(open(data_name + '_bottleneck_features_validation.npy', 'wb'), bottleneck_features_validation)
