from __future__ import print_function

import numpy as np
import random
import re

def char_dictionaries(text):
    chars = sorted(list(set(text)))
    print('Total chars:', len(chars))
    char_indices = dict((c, i) for i, c in enumerate(chars))
    indices_char = dict((i, c) for i, c in enumerate(chars))
    return char_indices, indices_char

def string_to_semi_redundant_sequences(text, char_indices, maxlen=25, step=3):
    sentences = []
    next_chars = []
    for i in range(0, len(text) - maxlen, step):
        sentences.append(text[i: i + maxlen])
        next_chars.append(text[i + maxlen])

    X = np.zeros((len(sentences), maxlen, len(char_indices)), dtype=np.bool)
    y = np.zeros((len(sentences), len(char_indices)), dtype=np.bool)
    for i, sentence in enumerate(sentences):
        for t, char in enumerate(sentence):
            X[i, t, char_indices[char]] = 1
        y[i, char_indices[next_chars[i]]] = 1
    return X, y

def sample(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)

def random_sequence_from_textfile(text, maxlen):
    start_index = random.randint(0, len(text) - maxlen - 1)
    sentence = text[start_index: start_index + maxlen]
    return sentence
    
def generate(model, maxlen, num_chars, char_indices, indices_char, seed, nb_samples=100, temperature=1.0, character_mode=True):    
    generated = seed if character_mode else list(seed)
    for i in xrange(nb_samples):
        # if generated.endswith('_END_'):
        # break
        x = np.zeros((1, maxlen, num_chars))
				
        for t, char in enumerate(seed):
            x[0, t, char_indices[char]] = 1.

        preds = model.predict(x, verbose=0)[0]
        next_index = sample(preds, temperature)
        next_char = indices_char[next_index]
					
        if character_mode:
            generated += next_char
            seed = seed[1:] + next_char
        else:
            generated.append(next_char)
            seed = seed[1:]
            seed.append(next_char)

    return generated if character_mode else ' ' .join(generated)

def convert_to_improvisor(chords):
    chords_improviser = []
    bar = []
    i = 0
    for chord in chords.split(' '):
        if chord == '_START_' or chord == '_END_':
            continue        
        if chord == 'C:6(9)':
            chord = 'C69'
        else:
            chord = chord.replace(':', '')
            chord = chord.replace('min', 'm')
            chord = chord.replace('maj', 'M')
            chord = chord.replace('/5', '')
            chord = chord.replace('h', '')
            chord = chord.replace('(b9)', 'b9')
            chord = chord.replace('(b5)', 'b5')
            chord = re.sub(r'\((\d)\)', r'\1', chord)            
            chord = re.sub(r'\([^()]+\)', '', chord)
            chord = re.sub(r'\/\d', '', chord)
        bar.append(chord)
        i += 1
        if i % 4 == 0:
            chords_improviser.append(' '.join(sorted(set(bar), key=bar.index)) + ',')
            bar = []        
    return ' '.join(chords_improviser)
